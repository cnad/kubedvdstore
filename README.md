# KubeMovieStore
This repo will use the pre built kubernetes image to
connect to a prebuilt kubernets cluster 
it will then create a postgres operator, 
a postgres cluster an insert data into the postgres enviroment.

This repository is setup to run this in gitlab runner and post to this the cluster. to run this on your desktop see [local desktop deployment](#local-deployment) 

# The Pipeline
The git lab pipeline uses our kubenetes configuration from the cluster

`- export KUBECONFIG=$kube_config`

it the creates a namespace of staging using the staging yaml in the repository
it then sets the context to staging and uses that context so we perform these 
commands ina  different enviroment. 

`- kubectl apply -f namespace-staging.yaml`

`- kubectl config set-context staging --namespace=staging --cluster=microk8s-cluster --user=admin`

`- kubectl config use-context staging`

Now we can install the cluster starting with the postgres operator

`- kubectl apply -k github.com/zalando/postgres-operator/manifests`

`- sleep 30`

-*- we sleep to wait for the process to be finished. maybe changed later -*-

once installed we apply our cluster yaml 

`- kubectl apply -f postgres-manifest.yaml`

`- sleep 30`

Once the cluster is created we need to put data into the cluster.
to this manually is challenging as you cannot enter the pod manually in gitlab cli 
instead we use two created scripts that populate the pod 
with our database. the frist script is bash script that executes the sql script
the first steps are to move our scripts into the pod
to add our data. we need to give permissions before copying so the 
script has permission to make changes to the pod.

`- chmod 774 createdb.sh`

with permission changed we then copy the files from the repo into the pod

`- kubectl cp createdb.sh acid-minimal-cluster-0:createdb.sh`

before we copy the schema we need to use sed to grab the password for the database
from our gitlab enviroment variables 

`- sed 's/%PASSWORD%/$schema_pw/g' schema.sql`

then we copy it 

`- kubectl cp schema.sql acid-minimal-cluster-0:schema.sql`

now we execute the script in the pod. 

`- kubectl exec --namespace staging acid-minimal-cluster-0 su postgres bash -- ./createdb.sh`

now we have succesfully deployed our cluster into the staging enviroment :)

# Local deployment 
to install locally clone this repository down and run the powershell script webstorelocaldeploy.ps1. 

### local setup differences to gitlab pipeline 
this is setup to be gitlab pipeline but it contains all the commands to be run locally to set up the web store. changes include 
- Stagging namespace and refrences removed
- account service .yaml does not need to be applied (staging namespace only step)
- removed bash from th execute command
- removed schema two as not needed
- deployed local postgres-manifest-local.yaml instead
- changed sh script to createdblocal.sh script

###  postgres-manifest-local.yaml
only the difference here is the change to namespace which is now set to default

### createdblocal.sh
changed the name of script to schema.sql as schematwo is not needed.

 
### schema.sql 
this is chnaged in the local pipeline using sed. however locally you will need to add in the correct password surround by single qoutes on line 10 in steand of the variable we declare %PASSWORD%

