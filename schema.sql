-- schema.sql
-- create user if does not exist this user is for the remote login from controller
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'simonr') THEN

      CREATE ROLE simonr LOGIN PASSWORD '%PASSWORD%';
      ALTER USER simonr WITH CREATEDB;
      ALTER USER simonr WITH CREATEROLE;
      ALTER USER simonr WITH SUPERUSER;
   END IF;
END
$do$;

-- Since we might run the import many times we'll drop if exists
DROP DATABASE IF EXISTS movie;

CREATE DATABASE movie;

-- Make sure we're using our `dvddb` database
\c movie;

-- We can create our user table
CREATE TABLE movie
(
  id serial NOT NULL,
  category_id integer NOT NULL,
  discs integer NOT NULL,
  media_type_id integer NOT NULL,
  region_id integer NOT NULL,
  title text,
  CONSTRAINT pk_movie PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--alter
ALTER TABLE movie
  OWNER TO postgres;

--insert data 
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (100,2,999,4,'Spiderman 2');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (100,2,999,4,'Spiderman Homecoming');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (100,2,999,4,'Avengers');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,6,996,4,'Code Geass: Lelouch of the Rebeliion');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,10,996,4,'Gundam Seed');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,10,996,4,'Gundam Seed Destiny');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,10,996,4,'Gundam 00');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,10,996,4,'Gundam Wing');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,10,996,4,'Gundam Iron Blood Orphans');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (110,6,998,4,'Stargate Season 10');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (110,6,998,4,'Stargate Atlantis Season 1');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (110,6,998,4,'Stargate Atlantis Season 2');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (110,6,998,4,'Stargate Destiny Season 1');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (105,6,998,4,'South Park Season 1');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (112,6,996,3,'Technoman');
insert into movie ( Category_id, Discs, Media_type_id, Region_id, Title) values (104,1,998,1,'24: Season Six preview');
