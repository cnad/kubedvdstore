kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/provider/cloud-generic.yaml
kubectl apply -k github.com/zalando/postgres-operator/manifests
kubectl rollout status deployment postgres-operator 
kubectl apply -f postgres-manifest-local.yaml 
sleep 20
kubectl wait --for=condition=ready pod/acid-minimal-cluster-0
kubectl cp createdb_local.sh acid-minimal-cluster-0:createdb_local.sh
(Get-content schema.sql ) | Foreach-Object {$_ -replace "%PASSWORD%", "password"} | Set-Content schema.sql
kubectl cp schema.sql acid-minimal-cluster-0:schema.sql
kubectl exec acid-minimal-cluster-0 su postgres -- ./createdb_local.sh
kubectl get pods 

